package com.piserve.geejo.extras;

/**
 * Created by Droid Space on 6/1/2015.
 */
public class UrlEndpoints {
    public static final String URL_SERVICE_BASE = "http://myservicekart.com/public/";
    public static final String URL_SEARCH_BASE = "http://myservicekart.com/public/search/";
    public static final String URL_CHAR_QUESTION = "?";
    public static final String URL_CHAR_AMPERSAND = "&";
    public static final String URL_PARAM_SEARCH = "search=";
    public static final String URL_PARAM_PAGENO = "pageno=";
    public static final String URL_CATEGORY = "http://myservicekart.com/public/category";
}
