package com.piserve.geejo.activities;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.piserve.geejo.extras.Constants;
import com.piserve.geejo.extras.Keys;
import com.piserve.geejo.logging.L;
import com.piserve.geejo.myservicekart.R;
import com.piserve.geejo.pojo.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import com.piserve.geejo.network.VolleySingleton;

import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_CATEGORY;
import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_SUB_CATEGORIES;
import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_SUB_CATEGORY_GROUP;
import static com.piserve.geejo.extras.UrlEndpoints.URL_CATEGORY;

public class CategoryActivity extends ActionBarActivity {

    private ArrayList<Category> mListCategories = new ArrayList<>();

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    public static String getCategoryRequestUrl() {
        return URL_CATEGORY;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        sendJsonCategoryRequest();
    }

    public void sendJsonCategoryRequest() {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (
                        Request.Method.GET,
                        getCategoryRequestUrl(),
                        (String) null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                mListCategories = parseJsonCategoryResponse(response);
                                L.t(getApplicationContext(), response.toString());
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );
        mRequestQueue.add(jsonObjectRequest);
    }

    public ArrayList<Category> parseJsonCategoryResponse(JSONObject response) {
        //ArrayList<Service> listServices = new ArrayList<>();
        ArrayList<Category> listCategories = new ArrayList<>();
        if (response != null && response.length() > 0) {
            long categoryId = -1;
            String categoryName = Constants.NA;
            String subCategoryGroupName = Constants.NA;
            String subCategoryName = Constants.NA;

            try {
                JSONArray categoriesArray = response.getJSONArray(KEY_CATEGORY);
                for (int i = 0; i < categoriesArray.length(); i++) {
                    JSONObject categoryObject = categoriesArray.getJSONObject(i);

                    if(categoryObject.has(Keys.EndpointCategory.KEY_ID) && !categoryObject.isNull(Keys.EndpointCategory.KEY_ID))
                        categoryId = categoryObject.getLong(Keys.EndpointCategory.KEY_ID);

                    if(categoryObject.has(Keys.EndpointCategory.KEY_NAME) && !categoryObject.isNull(Keys.EndpointCategory.KEY_NAME))
                        categoryName = categoryObject.getString(Keys.EndpointCategory.KEY_NAME);

                    JSONArray subCategoryGroupsArray = categoryObject.getJSONArray(KEY_SUB_CATEGORY_GROUP);
                    for(int j=0;j< subCategoryGroupsArray.length();j++){
                        JSONObject subCategoryGroupObject = subCategoryGroupsArray.getJSONObject(i);
                        if(subCategoryGroupObject.has(Keys.EndpointCategory.KEY_NAME) && !subCategoryGroupObject.isNull(Keys.EndpointCategory.KEY_NAME))
                            subCategoryGroupName = subCategoryGroupObject.getString(Keys.EndpointCategory.KEY_NAME);

                        JSONArray subCategoriesArray = subCategoryGroupObject.getJSONArray(KEY_SUB_CATEGORIES);
                        for(int k=0;k<subCategoriesArray.length();j++) {
                            JSONObject subCategoryObject = subCategoriesArray.getJSONObject(j);
                            if(subCategoryObject.has(Keys.EndpointCategory.KEY_NAME) && !subCategoryObject.isNull(Keys.EndpointCategory.KEY_NAME))
                                subCategoryName = subCategoryObject.getString(Keys.EndpointCategory.KEY_NAME);

                        }
                    }

                    Category category = new Category();
                    category.setId(categoryId);
                    category.setCategory(categoryName);
                    category.setSubCategoryGroup(subCategoryGroupName);
                    category.setCategory(subCategoryName);

                    if(categoryId!=1 && !categoryName.equals(Constants.NA))
                        listCategories.add(category);

                }

            } catch (JSONException e) {

            }
            L.t(getApplicationContext(), listCategories.size() + " rows fetched");
        }
        return listCategories;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
