package com.piserve.geejo.fragments;

/**
 * Created by Droid Space on 6/1/2015.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.android.volley.toolbox.JsonObjectRequest;
import com.piserve.geejo.activities.DetailActivity;
import com.piserve.geejo.adapters.AdapterAll;
import com.piserve.geejo.extras.Constants;
import com.piserve.geejo.extras.Keys;
import com.piserve.geejo.logging.L;
import com.piserve.geejo.myservicekart.MyApplication;
import com.piserve.geejo.myservicekart.R;
import com.piserve.geejo.network.VolleySingleton;
import com.piserve.geejo.myservicekart.RecyclerTouchListener;
import com.piserve.geejo.pojo.Category;
import com.piserve.geejo.pojo.Service;

import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_CATEGORY;
import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_SUB_CATEGORIES;
import static com.piserve.geejo.extras.Keys.EndpointCategory.KEY_SUB_CATEGORY_GROUP;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_ID;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_IMAGE_THUMBNAIL;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_NAME;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_ORGANIZATION_NAME;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_POSTED_USER;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_PRICE;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_PROFILE;
import static com.piserve.geejo.extras.Keys.EndpointService.KEY_RATING;
import static com.piserve.geejo.extras.UrlEndpoints.URL_CATEGORY;
import static com.piserve.geejo.extras.UrlEndpoints.URL_SEARCH_BASE;
import static com.piserve.geejo.extras.UrlEndpoints.URL_CHAR_QUESTION;
import static com.piserve.geejo.extras.UrlEndpoints.URL_PARAM_SEARCH;
import static com.piserve.geejo.extras.UrlEndpoints.URL_PARAM_PAGENO;
import static com.piserve.geejo.extras.UrlEndpoints.URL_CHAR_AMPERSAND;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentHome#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentHome extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    private ArrayList<Service> mListServices = new ArrayList<>();
    private AdapterAll mAdapterAll;
    private RecyclerView listServicesView;

    public FragmentHome() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentHome.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentHome newInstance(String param1, String param2) {
        FragmentHome fragmentHome = new FragmentHome();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragmentHome.setArguments(args);
        return fragmentHome;
    }

    public static String getRequestUrl() {

        return URL_SEARCH_BASE
                + URL_CHAR_QUESTION
                + URL_PARAM_SEARCH
                + URL_CHAR_AMPERSAND
                + URL_PARAM_PAGENO + MyApplication.HOME_PAGE_NUMBER;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();
    }

    private void sendJsonRequest() {
        JsonArrayRequest request = new JsonArrayRequest
                (
                        getRequestUrl(),
                        new Response.Listener<JSONArray>() {
                            @Override
                            public void onResponse(JSONArray response) {
                                mListServices = parseJSONResponse(response);
                                mAdapterAll.setServiceList(mListServices);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                L.t(getActivity(), error.getMessage() + "");
                            }
                        }
                );
        mRequestQueue.add(request);
    }

    private ArrayList<Service> parseJSONResponse(JSONArray response) {
        ArrayList<Service> listServices = new ArrayList<>();
        if (response != null && response.length() > 0) {
            long id = -1;
            String name = Constants.NA;
            String organizationName = Constants.NA;
            long price = -1;
            int rating = -1;
            String urlThumbnail = Constants.NA;

            try {

                for (int i = 0; i < response.length(); i++) {
                    JSONObject serviceObject = response.getJSONObject(i);
                    if (serviceObject.has(KEY_ID)
                            && !serviceObject.isNull(KEY_ID)) {
                        id = serviceObject.getLong(KEY_ID);
                    }

                    if (serviceObject.has(KEY_NAME)
                            && !serviceObject.isNull(KEY_NAME)) {
                        name = serviceObject.getString(KEY_NAME);
                    }

                    if(serviceObject.has(KEY_PRICE)
                            && !serviceObject.isNull(KEY_PRICE)) {
                        price = serviceObject.getLong(KEY_PRICE);
                    }

                    if (serviceObject.has(KEY_IMAGE_THUMBNAIL)
                            && !serviceObject.isNull(KEY_IMAGE_THUMBNAIL)) {
                        urlThumbnail = serviceObject.getString(KEY_IMAGE_THUMBNAIL);
                        urlThumbnail = urlThumbnail.replaceAll(" ", "+");
                    }

                    JSONObject postedUserObject = serviceObject.getJSONObject(KEY_POSTED_USER);
                    JSONObject profileObject = postedUserObject.getJSONObject(KEY_PROFILE);

                    if (profileObject != null && profileObject.has(KEY_RATING)
                            && !profileObject.isNull(KEY_RATING)) {
                        rating = profileObject.getInt(KEY_RATING);
                    }


                    if (profileObject != null && profileObject.has(KEY_ORGANIZATION_NAME)
                            && !profileObject.isNull(KEY_ORGANIZATION_NAME)) {
                        organizationName = profileObject.getString(KEY_ORGANIZATION_NAME);
                    }

                    Service service = new Service();
                    service.setId(id);
                    service.setName(name);
                    service.setOrganizationName(organizationName);
                    service.setPrice(price);
                    service.setRating(rating);
                    service.setUrlThumbnail(urlThumbnail);

                    if (id != -1 && !name.equals(Constants.NA)) {
                        listServices.add(service);
                    }

                }
            } catch (JSONException e) {

            }
            L.t(getActivity(), listServices.size() + " rows fetched");
        }
        return listServices;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        listServicesView = (RecyclerView) view.findViewById(R.id.listServicesView);
        listServicesView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapterAll = new AdapterAll(getActivity());
        listServicesView.setAdapter(mAdapterAll);
        sendJsonRequest();
        listServicesView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), listServicesView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent detailIntent = new Intent(getActivity(), DetailActivity.class);
                startActivity(detailIntent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        return view;
    }

    public static interface ClickListener{
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }
}

