package com.piserve.geejo.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.piserve.geejo.adapters.AdapterAll;
import com.piserve.geejo.extras.Constants;
import com.piserve.geejo.logging.L;
import com.piserve.geejo.myservicekart.R;
import com.piserve.geejo.network.VolleySingleton;


import com.piserve.geejo.pojo.Category;
import com.piserve.geejo.pojo.Service;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.piserve.geejo.extras.UrlEndpoints.*;
import static com.piserve.geejo.extras.Keys.EndpointCategory.*;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NavigatedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NavigatedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NavigatedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RequestQueue mRequestQueue;
    private VolleySingleton mVolleySingleton;

    private ArrayList<Service> mListServices = new ArrayList<>();
    private ArrayList<Category> mListCategories = new ArrayList<>();
    private AdapterAll mAdapterAll;
    private RecyclerView listServicesView;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NavigatedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NavigatedFragment newInstance(String param1, String param2) {
        NavigatedFragment fragment = new NavigatedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static String getCategoryRequestUrl() {
        return URL_CATEGORY;
    }


    public NavigatedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        mVolleySingleton = VolleySingleton.getInstance();
        mRequestQueue = mVolleySingleton.getRequestQueue();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.hello_blank_fragment);
        return textView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void sendJsonCategoryRequest() {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (
                        Request.Method.GET,
                        getCategoryRequestUrl(),
                        (String) null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                mListCategories = parseJsonCategoryResponse(response);
                                L.t(getActivity(), response.toString());
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );
        mRequestQueue.add(jsonObjectRequest);
    }

    public ArrayList<Category> parseJsonCategoryResponse(JSONObject response) {
        //ArrayList<Service> listServices = new ArrayList<>();
        ArrayList<Category> listCategories = new ArrayList<>();
        if (response != null && response.length() > 0) {
            long categoryId = -1;
            String categoryName = Constants.NA;
            String subCategoryGroupName = Constants.NA;
            String subCategoryName = Constants.NA;

            try {
                JSONArray categoriesArray = response.getJSONArray(KEY_CATEGORY);
                for (int i = 0; i < categoriesArray.length(); i++) {
                    JSONObject categoryObject = categoriesArray.getJSONObject(i);

                    if(categoryObject.has(KEY_ID) && !categoryObject.isNull(KEY_ID))
                        categoryId = categoryObject.getLong(KEY_ID);

                    if(categoryObject.has(KEY_NAME) && !categoryObject.isNull(KEY_NAME))
                        categoryName = categoryObject.getString(KEY_NAME);

                    JSONArray subCategoryGroupsArray = categoryObject.getJSONArray(KEY_SUB_CATEGORY_GROUP);
                    for(int j=0;j< subCategoryGroupsArray.length();j++){
                        JSONObject subCategoryGroupObject = subCategoryGroupsArray.getJSONObject(i);
                        if(subCategoryGroupObject.has(KEY_NAME) && !subCategoryGroupObject.isNull(KEY_NAME))
                            subCategoryGroupName = subCategoryGroupObject.getString(KEY_NAME);

                        JSONArray subCategoriesArray = subCategoryGroupObject.getJSONArray(KEY_SUB_CATEGORIES);
                        for(int k=0;k<subCategoriesArray.length();j++) {
                            JSONObject subCategoryObject = subCategoriesArray.getJSONObject(j);
                            if(subCategoryObject.has(KEY_NAME) && !subCategoryObject.isNull(KEY_NAME))
                            subCategoryName = subCategoryObject.getString(KEY_NAME);

                        }
                    }

                    Category category = new Category();
                    category.setId(categoryId);
                    category.setCategory(categoryName);
                    category.setSubCategoryGroup(subCategoryGroupName);
                    category.setCategory(subCategoryName);

                    if(categoryId!=1 && !categoryName.equals(Constants.NA))
                        listCategories.add(category);

                }

            } catch (JSONException e) {

            }
            L.t(getActivity(), listCategories.size() + " rows fetched");
        }
        return listCategories;
    }
}
