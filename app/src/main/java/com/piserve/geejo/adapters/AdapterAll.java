package com.piserve.geejo.adapters;

/**
 * Created by Droid Space on 6/1/2015.
 */
import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

import com.piserve.geejo.extras.Constants;
import com.piserve.geejo.myservicekart.R;
import com.piserve.geejo.network.VolleySingleton;
import com.piserve.geejo.pojo.Service;

public class AdapterAll extends RecyclerView.Adapter<AdapterAll.ViewHolderAll> {

    private ArrayList<Service> mListServices = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private VolleySingleton volleySingleton;
    private ImageLoader imageLoader;

    public AdapterAll(Context context) {
        layoutInflater = LayoutInflater.from(context);
        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();

    }

    public void setServiceList(ArrayList<Service> listServices) {
        this.mListServices = listServices;
        notifyItemRangeChanged(0, listServices.size());
    }

    @Override
    public ViewHolderAll onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.custom_list_all, parent, false);
        ViewHolderAll viewHolder = new ViewHolderAll(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderAll holder, int position) {
        Service currentService = mListServices.get(position);
        holder.serviceName.setText(currentService.getName());

//        holder.serviceOrganizationName.setPaintFlags(holder.serviceOrganizationName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        holder.serviceOrganizationName.setText(currentService.getOrganizationName());
        holder.servicePrice.setText(Long.toString(currentService.getPrice()));

        int rating= currentService.getRating();
        if(rating==-1){
            holder.ratingBarScore.setRating(0.0F);
            holder.ratingBarScore.setAlpha(0.5F);
        }
        else{
            holder.ratingBarScore.setRating(rating / 20.0F);
            holder.ratingBarScore.setAlpha(1.0F);
        }

        String urlThumnail = currentService.getUrlThumbnail();
        loadImages(urlThumnail, holder);

    }

    private void loadImages(String urlThumbnail, final ViewHolderAll holder) {
        if (!urlThumbnail.equals(Constants.NA)) {
            imageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.serviceThumbnail.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mListServices.size();
    }

    static class ViewHolderAll extends RecyclerView.ViewHolder {

        ImageView serviceThumbnail;
        TextView serviceName;
        TextView serviceOrganizationName;
        TextView servicePrice;
        RatingBar ratingBarScore;

        public ViewHolderAll(View itemView) {
            super(itemView);
            serviceThumbnail = (ImageView) itemView.findViewById(R.id.serviceThumbnail);
            serviceName = (TextView) itemView.findViewById(R.id.serviceName);
            serviceOrganizationName = (TextView) itemView.findViewById(R.id.serviceOrganizationName);
            servicePrice = (TextView) itemView.findViewById(R.id.servicePrice);
            ratingBarScore = (RatingBar) itemView.findViewById(R.id.serviceRatingScore);
        }
    }
}
