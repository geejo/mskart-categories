package com.piserve.geejo.pojo;

/**
 * Created by droidspace on 4/6/15.
 */
public class Category {

    long id;
    private String category;
    private String name;
    private String subCategoryGroup;

    public Category(){
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubCategoryGroup() {
        return subCategoryGroup;
    }

    public void setSubCategoryGroup(String subCategoryGroup) {
        this.subCategoryGroup = subCategoryGroup;
    }

    public String toString(){
        return "ID: " +id +
               "Name: " +name +
               "\n";
    }
}
